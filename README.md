# Apache Spark Python Framework Machine Learning Data Pipeline

Natural Language Processing project for sentiment classification of movie reviews. Implemented in Python using Apache Spark framework i.e. pyspark library.

The Large Movie Review Dataset (https://ai.stanford.edu/~amaas/data/sentiment/) is used and has to have directory structure as defined in "dataset_struct.txt".

My lab assignment in Networked Systems Middleware, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: Jan 2021